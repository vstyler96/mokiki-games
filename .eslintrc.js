const ENV = process.env.NODE_ENV || "development";

module.exports = {
  root: true,
  env: {
    browser: true,
    es6: true,
    node: true,
  },
  extends: [
    "plugin:vue/vue3-essential",
  ],
  rules: {
    "no-console": "off",
    camelcase: "off",
    quotes: ["error", "double"],
    "no-extra-semi": "error",
    "no-debugger": ENV === "production" ? "error" : "off",
    "max-len": ["error", { code: 255 }],
    "no-param-reassign": "off",
    "global-require": "off",
    "import/no-unresolved": "off",
    "import/order": "off",
    "import/extensions": "off",
    "arrow-body-style": "off",
    "arrow-parens": ["error", "as-needed"],
    "linebreak-style": "off",
    "func-names": "off",
    "vue/component-name-in-template-casing": ["error", "PascalCase", { registeredComponentsOnly: true }],
    "vue/multi-word-component-names": "off",
    "no-plusplus": "off",
  },
  parserOptions: {
    ecmaVersion: 12,
    parser: "@babel/eslint-parser",
    requireConfigFile: false,
    sourceType: 'module',
  },
  globals: {
    Echo: true,
    Pusher: true,
  },
};
