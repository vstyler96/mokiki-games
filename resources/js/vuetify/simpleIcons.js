import { h } from "vue";
import SimpleIcon from "@/components/SimpleIcon.vue";

const aliases = {
  discord: "si:siDiscord",
  steam: "si:siSteam",
  linkedin: "si:siLinkedin",
};

const si = {
  component: ({ icon }) => h(SimpleIcon, {
    icon,
  }),
};

export { aliases, si };
