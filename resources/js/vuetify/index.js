// Vuetify
import { createVuetify } from "vuetify";
import { md3 as blueprint } from "vuetify/blueprints";
import themes from "./themes";

import { si } from "./simpleIcons";

// Components and directives
import * as components from "vuetify/components";
import * as directives from "vuetify/directives";

// Styles
import "@mdi/font/css/materialdesignicons.css";
import "./variables.scss";

export default createVuetify({
  blueprint,
  components,
  directives,
  theme: {
    defaultTheme: "dark",
    variations: {
      colors: ["primary", "secondary"],
      lighten: 3,
      darken: 6,
    },
    themes,
  },
  icons: {
    sets: { si },
  },
  display: {
    mobileBreakpoint: "sm",
  },
});
