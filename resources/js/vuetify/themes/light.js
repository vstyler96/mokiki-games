export default {
  dark: true,
  colors: {
    primary: "#662D91",
    secondary: "#ec008c",
    accent: "#25bbaf",
  },
};
