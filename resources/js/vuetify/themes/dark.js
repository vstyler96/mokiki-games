export default {
  dark: true,
  colors: {
    primary: "#E1B6FF",
    secondary: "#FFB0CC",
    accent: "#51DBCE",
    background: "#1D1B1E",
    surface: "#1D1B1E",
  },
};
