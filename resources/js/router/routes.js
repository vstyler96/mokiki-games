const leading = [
  {
    path: "/",
    name: "welcome",
    alias: "/welcome",
    component: () => import("@/views/Welcome/index.vue"),
    meta: {
      title: "Welcome",
      icon: "mdi-home",
    },
  },
  {
    path: "/about",
    name: "welcome.about",
    component: () => import("@/views/About.vue"),
    meta: {
      title: "About",
      icon: "mdi-information",
    },
  },
];

const trailing = [
  {
    path: "/contact",
    name: "welcome.contact",
    component: () => import("@/views/Contact.vue"),
    meta: {
      title: "Contact",
      avatar: "/images/favicon.png",
      // icon: "mdi-phone",
    },
  },
];


const addons = [
  {
    path: "/tree",
    name: "welcome.tree",
    component: () => import("@/views/Tree.vue"),
    meta: {
      title: "Tree Info",
      icon: "mdi-tree",
    },
  },
  {
    path: "/privacy-terms",
    name: "welcome.privacy",
    component: () => import("@/views/Privacy.vue"),
    meta: {
      title: "Privacy terms",
      icon: "mdi-vpn",
    },
  },
];

const main = [
  ...leading,
  ...trailing,
];

export default [
  ...leading,
  ...trailing,
  ...addons,
];
export { main, leading, trailing };
