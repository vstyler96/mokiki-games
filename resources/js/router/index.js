import { createRouter, createWebHistory } from "vue-router";
import routes from "./routes";

const router = createRouter({
  history: createWebHistory(),
  routes,
});

router.beforeEach(to => {
  const { meta: { title } } = to;
  document.title = `${import.meta.env.VITE_APP_NAME} :: ${title}`;
});

export default router;
