import {
  Form,
  Field,
  defineRule,
  configure,
} from "vee-validate";
import rules from "@vee-validate/rules";
import { localize } from "@vee-validate/i18n";
import en from "@vee-validate/i18n/dist/locale/en.json";
import es from "@vee-validate/i18n/dist/locale/es.json";

configure({
  classes: {
    valid: "success",
    invalid: "error",
  },
  generateMessage: localize({ en, es }),
});

Object.keys(rules).forEach(rule => {
  defineRule(rule, rules[rule]);
});

export { Form, Field };
export default { Form, Field };
