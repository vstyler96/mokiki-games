
import { create } from "axios";

const axios = create({
  baseURL: "api",
  headers: { "X-Requested-With": "XMLHttpRequest" }
});

export default axios;