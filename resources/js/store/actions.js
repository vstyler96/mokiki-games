import axios from "axios";

export default {
  async sendEmail({ commit }, data) {
    try {
      const { data: { message } } = await axios.post("/api/send-email", data);
      commit("setAlert", { message, type: "success" }, { root: true });
    } catch({ response: { data: { message } } }) {
      commit("setAlert", { message, type: "warning" }, { root: true });
    }
  },
};
