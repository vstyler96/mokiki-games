import actions from "./actions";

export default {
  state() {
    return {
      alerts: [],
    };
  },
  mutations: {
    setAlert(state, alert) {
      state.alerts.push(alert);
      setTimeout(() => {
        state.alerts.shift();
      }, 2000);
    },
  },
  actions,
};
