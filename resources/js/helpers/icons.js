import {
  siDiscord,
  siSteam,
  siMastercard,
  siLinkedin,
  siTiktok,
  siItchdotio,
} from "simple-icons/icons";

export default {
  siDiscord,
  siSteam,
  siMastercard,
  siLinkedin,
  siTiktok,
  siItchio: siItchdotio,
};
