import { createApp } from "vue";

// Plugins
import vuex from "./store";
import router from "./router";
import vuetify from "./vuetify";
import { Form, Field } from "./veeValidate";

// Aplication entry.
import App from "./App.vue";
import { VueReCaptcha } from "vue-recaptcha-v3";

const app = createApp(App);

app.use(router);
app.use(vuex);
app.use(vuetify);
app.use(VueReCaptcha, { siteKey: import.meta.env.VITE_RECAPTCHA_SITE_KEY });

app.component("VeeForm", Form);
app.component("VeeField", Field);

app.mount("#app");
