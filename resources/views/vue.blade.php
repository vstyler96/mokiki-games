<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <title>{{ config("app.name") }}</title>
    <link rel="shortcut icon" href="{{ asset("images/favicon.png") }}" type="image/png">
    <link href="https://fonts.googleapis.com/css2?family=Lato:wght@300;400&family=Nunito:wght@200;300;400;700&display=swap" rel="stylesheet">
</head>
<body>
    <div id="app"></div>

    <!-- Vite assets -->
    @vite("resources/js/main.js")
</body>
</html>