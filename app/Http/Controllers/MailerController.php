<?php

namespace App\Http\Controllers;

use App\Models\Message;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;

class MailerController extends Controller
{
    public function sendEmail(Request $request): JsonResponse
    {
        $validated = $request->validate([
            "from" => "required|email",
            "to"   => "required|email",
            "subject" => "required|in:Budget,Internship,Job,Appointment",
            "message" => "required|min:20|max:150",
            "g-recaptcha-response" => "required|recaptchav3:email,0.5",
        ], [
            "g-recaptcha-response.recaptchav3" => "El captcha es inválido.",
        ]);

        Message::firstOrCreate(Arr::except($validated, ["g-recaptcha-response"]), [
            "remote_ip" => $request->server("REMOTE_ADDR"),
            "remote_forwarded" => $request->server("HTTP_X_FORWARDED_FOR"),
        ]);

        return response()->json([ "message" => "Message successfully sent." ]);
    }
}
