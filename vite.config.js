import { defineConfig } from "vite";
import laravel from "laravel-vite-plugin";
import vue from "@vitejs/plugin-vue";
import vuetify from "vite-plugin-vuetify";
import eslint from "vite-plugin-eslint";

const fs = require("fs");

export default defineConfig({
  build: {
    chunkSizeWarningLimit: 1024,
  },
  /* server: {
    host: "mokikigames.app.test",
    https: {
      cert: fs.readFileSync("/srv/certificates/localhost.crt"),
      key: fs.readFileSync("/srv/certificates/localhost.key"),
    },
  }, */
  resolve: {
    alias: {
      "@": `${__dirname}/resources/js`,
    },
  },
  plugins: [
    eslint(),
    vue({
      template: {
        transformAssetUrls: {
          includeAbsolute: false,
        },
      },
    }),
    vuetify({ styles: "expose" }),
    laravel({
      input: [ "resources/js/main.js" ],
      refresh: true,
    }),
  ],
});
