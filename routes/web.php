<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Place your custom views here, a PDF for example.

// Route::get("/pdf/testing", function () {
//     return "PDF Example!";
// });

// Route::view("/testing/email/subscription", "mail.subscription.registered");

// Place this at the very bottom of your routes.
Route::view("{route?}", "vue")->where("route", ".*");